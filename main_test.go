package main

import (
	"net/http"
	"net/url"
	"testing"

	"github.com/Pallinder/go-randomdata"
)

func BenchmarkPosts(b *testing.B) {
	for i := 0; i < b.N; i++ {
		res, err := http.PostForm("http://127.0.0.1:8000/post", url.Values{"post": {randomdata.Paragraph()}})
		if err != nil {
			b.Fatal(err)
		}
		if res.StatusCode != 200 {
			b.Fail()
		}
	}
}
func BenchmarkLoadSpeed(b *testing.B) {
	_, err := http.Get("http://127.0.0.1:8080/post")
	if err != nil {
		b.Fail()
	}
}
