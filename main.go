package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/alecthomas/chroma/quick"

	m "git.drk.sc/bluefirecorp/snip/models"

	"github.com/gorilla/mux"
)

//Posts are the user posts.
var Posts = []m.Post{}

func main() {

	fmt.Println("Initalizing forum software")

	mr := mux.NewRouter() // mux router

	srv := &http.Server{
		Handler: mr,
		Addr:    "0.0.0.0:8000",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second, // use a different server for larger content.
		ReadTimeout:  15 * time.Second,
	}

	mr.HandleFunc("/", homeHandler)
	mr.PathPrefix("/post").Methods("GET").HandlerFunc(getPost)          // returns list of posts
	mr.PathPrefix("/post").Methods("POST", "PUT").HandlerFunc(makePost) // appends list of posts

	srv.ListenAndServe()
}

func homeHandler(rw http.ResponseWriter, r *http.Request) {
	rw.Write([]byte("Under dev..."))
}

func makePost(rw http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		fmt.Fprint(rw, err)
	}

	Posts = append(Posts, m.NewPost(r.FormValue("post")))
	fmt.Fprintf(rw, "Success")
}

func getPost(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(rw, Form)
	for _, y := range Posts {
		quick.Highlight(rw, y.Text, "python", "html", "monokai")
		fmt.Println(y.Text)
		//j, _ := json.Marshal(y)
		//fmt.Fprintf(rw, string(j))
	}

}

//Form is input form
var Form = `<html>
<head></head>
<body>
<form action='/post' method="POST">
<textarea name='post' id='post'></textarea>
<input type='submit'</input>
</form>
`
