package models

import (
	"time"
)

//PostID is the current post ID, it's incremented while adding post
var PostID uint64

//Post is an object model representing posts
type Post struct {
	ID       uint64
	Text     string
	PostDate time.Time
}

//NewPost creates a new post
func NewPost(s string) Post {
	PostID++
	return Post{
		ID:       PostID,
		Text:     s,
		PostDate: time.Now(),
	}
}
